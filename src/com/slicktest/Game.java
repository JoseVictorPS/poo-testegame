package com.slicktest;

import org.newdawn.slick.*;

import java.util.ArrayList;

public class Game extends BasicGame {
    //Nave do jogador
    private Ship Ship;

    //Controlador de tiros
    private Gun gun;

    //Altura e largura da janela
    private int wHeight = 600, wWidth = 800;

    //Lista de imagem de vidas para mostrar na tela
    private ArrayList<Image> lives;

    //Controlador de asteroides
    private AsteroidController asteroidController;

    //Inicia o jogo no nível 1
    private int nivel = 1;

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException{
        //Desenha a nave
        Ship.getImg().draw(Ship.getX(), Ship.getY());

        //Espaço entre os corações "lives"
        int esp=8;
        //Desenha os corações com espaçamento
        for( Image l : lives) { l.draw(esp+1,30); esp+=20; }

        //Renderiza todas as balas
        gun.renderBullets(gc, g);

        //Renderiza cada um dos asteroides
        float astx, asty;
        for(Asteroid a : asteroidController.getAsteroides()) {
            astx = a.getX();
            asty = a.getY();
            a.getImg().draw(astx, asty);
        }

        //Desenha marcador de pontuação na tela
        g.drawString("SCORE: " + asteroidController.getScore(), 9, 55);
    }

    @Override
    public void update(GameContainer gc, int t) throws SlickException{

        //Controla movimento da nave
        Ship.Joystick(gc, t);

        //Testa colisão da nave com os asteroides
        asteroidController.crashShip(Ship, lives);

        //Controla tiro da nave
        gun.gunFire(gc, t);

        //Atualiza balas
        gun.updateBullets(t);

        //Testa destruição dos asteroides
        asteroidController.smashAsteroids(gun.getBullets());

        //Atualiza posição dos asteroides
        asteroidController.moveAsteroids(t);
    }

    @Override
    public void init(GameContainer gc) throws SlickException{
        //Inicializa nave
        Ship = new Ship("/images/nave.png",wWidth,wHeight);

        //Inicializa arma do jogador
        gun = new Gun(Ship);

        //Inicializa lista de vidas do jogador
        lives = new ArrayList<>();

        //Adiciona vidas na lista de vidas baseado na vida atual do jogador
        int i;
        for(i=Ship.getLife()-1; i>=0; i--)lives.add(new Image("/images/vida.png"));

        //Inicia controlador de asteroides
        asteroidController = new AsteroidController(nivel);

        //Liga os asteroides
        asteroidController.turnOnAsteroids();
    }

    //Batiza a janela de Asteroides
    public Game () { super("Asteroides"); }
}
