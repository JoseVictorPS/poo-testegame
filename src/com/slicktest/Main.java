package com.slicktest;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Main {

    public static void main(String[] args) {

        try {
            //Inicia classe que controla fluxo de jogo
            AppGameContainer app = new AppGameContainer(new Game());

            //Atribui modo como a janela será mostrada
            app.setDisplayMode(800, 600, false);

            //Fixa relação frame por segundo
            app.setTargetFrameRate(60);

            //Inicia o fluxo de jogo
            app.start();

        }catch (SlickException e) { e.printStackTrace(); }
    }
}