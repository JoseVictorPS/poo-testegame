package com.slicktest;

import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;
import java.util.LinkedList;
import org.newdawn.slick.Image;

public class AsteroidController {
    //Lista para controlar asteroides
    private LinkedList<Asteroid> asteroides;

    //Vetor de posição dos asteroides
    private Vector2f[] pos;

    //Nível de dificuldade
    private int nivel;

    //Altura e largura das janelas
    private int wHeight=600, wWidth=800;

    //Diametros dos tipos diferentes de asteroides
    private int d1, d2, d3;

    //Pontuação ao destruir asteroides
    private int score;

    public AsteroidController(int n) {
        //Inicia lista de asteroides
        asteroides = new LinkedList<>();

        //Atribui nível ao parametro n
        nivel = n;

        //O jogo iniciará com 6 asteroides na tela
        pos = new Vector2f[6];

        d1 = 50; d2 = 30; d3 = 14;
        score = 0;
    }

    public LinkedList<Asteroid> getAsteroides() {
        return asteroides;
    }

    public int getScore() { return score; }

    //Método para "ligar" os asteroides
    public void turnOnAsteroids() {
        //Percorre o vetor de posições
        for(int i=5; i>=0; i--) {

            //Gera uma posição aleatória para cada asteroide iniciar
            pos[i] = genPosition(i);

            //Cria um asteroide nessa posição aleatória
            insert(new MaiorAsteroid(pos[i]));
        }
    }

    //Método para inserir asteroides à lista de asteroides
    private void insert(Asteroid a) {
        asteroides.add(a);
    }

    //Método para remover asteróides da lista de asteroides
    private void remove(Asteroid a) {
        asteroides.remove(a);
    }

    //Método para gerar posições aleatórias
    private Vector2f genPosition(int i) {
        Vector2f pos;
        //Um ponto aleatório entre o topo e o chão da janela
        float height = (float)Math.random()*wHeight;
        float width;

        //Para os 3 primeiros asteroides, no canto esquerdo da tela
        if(i < 3) width = (float)Math.random()* (wWidth/3);

        //Para os 3 últimos, o canto direito da tela
        else width = (float)Math.random()*(wWidth/3) + (wWidth*2/3);

        pos = new Vector2f(width, height);
        return pos;
    }

    //Método para mover os asteroides passando deltatime
    public void moveAsteroids(int t) {
        for(Asteroid a : asteroides) a.naturalMove(t);
    }

    //Método que testa se o jogador acertou os asteroides
    public void smashAsteroids(ArrayList<Bullet> bullets) {
        //Itera pelos asteroides, com remoção
        for(int i = asteroides.size()-1; i>=0; i--) {

            //Recebe o asteroide da atual iteração
            Asteroid a = asteroides.get(i);

            //Itera pela lista de balas
            for(Bullet b : bullets) {

                //Testa colisão de circulos entre asteroide e bala
                if (Colision.circle(b.getCenter(),
                        a.getCenter(), b.getRadius(), a.getRadius())) {

                    //Atinge o asteroide
                    a.gotHit();

                    //Desliga a bala
                    b.inactivate();
                }
            }

            //Se o asteroide for destruído
            if(!a.isAlive()) {

                scorePoints(a);

                //Método para criar os asteroides menores, se houver
                makeSmallerAsteroid(a, asteroides.size());

                //Remove o asteroide
                asteroides.remove(i);
            }
        }
    }

    private void makeSmallerAsteroid(Asteroid a, int size) {
        if(a instanceof MaiorAsteroid) {
            Vector2f pos1 = new Vector2f(a.getX(),
                    a.getY() + a.getRadius());
            asteroides.add(size,
                    new MedAsteroid(pos1, a.getAngle(), true));

            Vector2f pos2 = new Vector2f(a.getX() + 30,
                    a.getY          () + a.getRadius());
            asteroides.add(size,
                    new MedAsteroid(pos2, a.getAngle(), false));
        }
        else if (a instanceof MedAsteroid) {
            Vector2f pos1 = new Vector2f(a.getX(),
                    a.getY() + a.getRadius());
            asteroides.add(size,
                    new PeqAsteroid(pos1, a.getAngle(), true));

            Vector2f pos2 = new Vector2f(a.getX() + 30,
                    a.getY          () + a.getRadius());
            asteroides.add(size,
                    new PeqAsteroid(pos2, a.getAngle(), false));
        }
    }

    private void scorePoints(Asteroid a) {
        if(a instanceof MaiorAsteroid) score += 200;
        else if (a instanceof MedAsteroid) score +=100;
        else if (a instanceof PeqAsteroid) score += 50;
    }

    public void crashShip(Ship ship, ArrayList<Image> lives) {
        //Percorre lista de asteroides testando colisão com a nave
        for(int i = asteroides.size()-1; i>=0; i--) {

            //Recebe o asteroide atual da iteração
            Asteroid a = asteroides.get(i);

            //Se a nave colidiu com o asteroide
            if(Colision.circleTriangle(ship, a)) {

                //Retira uma vida da nave
                ship.gotHit(lives);

                //Remove o asteroide
                asteroides.remove(i);
            }
        }
    }

}
