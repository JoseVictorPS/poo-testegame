package com.slicktest;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

public class Bullet {
    //Posição da bala
    private Vector2f pos;

    //Velocidade da bala
    private Vector2f speed;

    //Tempo em que a bala está ativa
    private int lived = 0;

    //Diametro da imagem da bala
    private int d = 6;

    //Se a bala está ativa
    private boolean activ = true;

    //Tempo máximo que bala pode estar ativa
    private static int MAX_LIFETIME = 1800;

    public Bullet (Vector2f pos, Vector2f speed) {
        this.pos = pos;
        this.speed = speed;
    }

    //Atualização por frame da bala
    public void update (int t) {
        //Se ela está ativa
        if (activ) {

            //atualiza a posição caso ela atravesse a janela
            pos=Colision.crossWindow(pos,d);

            //Velocidade real escalonada com o deltatime
            Vector2f realSpeed = speed.copy();
            realSpeed.scale( (t/1000.0f) );

            //Soma o vetor posição com a velocidade real
            pos.add(realSpeed);

            //Soma o deltatime ao tempo ativa
            lived += t;

            //Se está ativa por mais tempo do que o permitido
            if(lived > MAX_LIFETIME) inactivate();
        }
    }

    //Método de renderização das balas
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (isActiv())
        {
            //A cor da bala é vermelha
            g.setColor(Color.red);

            //Formato circular, pois largura e altura são as mesmas
            g.fillOval(pos.getX() - 6, pos.getY() - 6, 6, 6);
        }
    }

    public boolean isActiv() { return activ; }
    public float getPosX() { return pos.x; }
    public float getPosY() { return pos.y; }
    public int getDiameter() { return d; }
    public Vector2f getPos() { return pos; }
    public int getRadius() { return d/2; }
    public void inactivate() { activ = false; }
    public Vector2f getCenter() {
        return new Vector2f(pos.x+3, pos.y+3);
    }

}
