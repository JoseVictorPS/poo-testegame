package com.slicktest;

import org.newdawn.slick.geom.Vector2f;

public class MaiorAsteroid extends Asteroid{
    public MaiorAsteroid(Vector2f v) {
        //Passa o vetor posição
        super(v);

        //Atribui a imagem para o asteroide maior
        setImg("/images/maiorast.png");

        //Atribui a vida do asteroide
        setLife(7);

        //Atribui o diametro da imagem
        setD(50);

        //Cada asteroide começa se movendo numa direção aleatória
        setSpeed(genSpeed(genRandomAngle(), 80));
    }

    private double genRandomAngle() {
        //Inicia valor 2Pi
        double twopi = 2*Math.PI;

        //Gera um valor aleatório entre 0 e 2Pi(360º)
        setAngle(Math.random()*twopi);
        return getAngle();
    }

}
